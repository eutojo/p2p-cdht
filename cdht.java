import java.io.*;
import java.net.*;
import java.util.*;

/*
 * CDHT: Main class of the CDHT
 * Responsible for intitiating the CDHT according to user input and deciphering user requests
 * 	such as request and quit.
 */

public class cdht {

   public static void main(String[] args) throws Exception {
	  // Get command line argument.
	  // Two arguments required
	  if (args.length != 3) {
		 System.out.println("Required arguments: identity peer peer");
		 return;
	  }

	  // Set up the Peer ID and the peer's successors
	  int peerID =  Integer.parseInt(args[0]);
	  int successor1 = Integer.parseInt(args[1]);
	  int successor2 = Integer.parseInt(args[2]);

	  // Create an instance of the peer
	  Peer peer = new Peer(peerID, successor1, successor2);

	  // Keep a while loop running to obtain user input
	  while(true){
		// Obtain user input
		 Scanner sc=new Scanner(System.in);
		 String input = sc.nextLine();

		 // Split the string according to spaces
		 String splitInput[] = input.split(" ");

		 // Check if the peer is making a request:
		 // Note: Assumes valin request/quit input
		 if(input.contains("request")){
			int fileNumber = Integer.parseInt(splitInput[splitInput.length-1]);
			peer.fileRequest(fileNumber);

		 // Check if the peer is leaving the DHT
		 } else if (input.contains("quit")){
			peer.closePeer();
		 }
		 
	  }

   }

}

/*
 * PingRequest: The thread which is responsible for the pinging of successors.
 * Ping requests are sent around every 7 seconds. The peer will continue pinging its 
 *	successors unless it quits. 
 * This class is also responsible for determining whether a peer has been killed.
 */   

class PingRequest extends Thread{
	private Peer peer;		// The peer that is making the ping requests
	private int run;		// Variable which determines whether ping should keep running
	private int pingNo1;	// Number of pings received from successor 1
	private int pingNo2;	// Number of pings received from successor 2
	private int sentPing1;	// Number of pings sent to successor 1
	private int sentPing2;	// Number of pings sent to successor 2

	PingRequest(Peer peer) throws Exception{
		// Initiating the ping thread fields
		this.peer = peer;
		this.run = 1;
		this.pingNo1 = 0;
		this.pingNo2 = 0;
		this.sentPing1 = 0;
		this.sentPing2 = 0;
	}

   public void run() {
   		// Keep running until the peer departs the CDHT
		while(this.run == 1){
			// Flag used to determine if a peer has been killed
			int modified = 0;

			try {
			//Set up datagram message to be sent to successor 1
			// InetAddress is simply local host -> local machine
			InetAddress host = InetAddress.getByName("127.0.0.1");
			// Ping message format
			String output = "PING " + this.peer.getPeerID() + " 1" + "\n";
			
			// Set up datagram packet for successor 1
			byte[] buf = new byte[1024];
			buf = output.getBytes();
			// Get the port number to be sent to
			int port = this.peer.getSuccessor1() + 5000;
			// Package the ping
			DatagramPacket ping = new DatagramPacket(buf, buf.length, host, port);
			
			// Send ping to successor 1
			this.peer.getUDPSocket().send(ping);
			// Iincrement the number of pings sent to successor 1
			this.sentPing1 = this.sentPing1 + 1;

			// Modify the message to be sent to successor 2
			output = "PING " + this.peer.getPeerID() + " 2" + "\n";
			
			// Repeat the process, similar to sending a ping packet to
			//	successor 1
			buf = output.getBytes();
			port = this.peer.getSuccessor2() + 5000;
			ping = new DatagramPacket(buf, buf.length, host, port);
			
			this.peer.getUDPSocket().send(ping);
			this.sentPing2 = this.sentPing2+ 1;

			// Wait 7 seconds before sending another ping packet
			sleep(10000);

				// Determine whether a peer has been killed
				try {
					// Compare the number of pings sent to the number of pings received.
					//	If the difference is at 3, the peer has been killed.
					// Note: The number of pings is incremented in the UDP thread
					// For successor 1:
					if (this.sentPing1 > (this.getPingNo1() + 2)){
						// Print out the require message
						System.out.println("Peer " + this.peer.getSuccessor1() + " has been killed.");
						
						// The second successor is now the first successor.
						System.out.println("My first successor is now peer " + this.peer.getSuccessor2());
						
						// Update the peer's first successor
						this.peer.setSuccessor1(this.peer.getSuccessor2());
						
						// Find the peer's new second successor
						this.peer.getNewSuccessor2(this.peer.getSuccessor2());
						
						// Set the modified flag to 1 since the peers has had its successors changed
						modified = 1;

					} 

					// Repeat the aboe check for successor 2
					if (this.sentPing2 > (this.getPingNo2() + 2)){
						System.out.println("Peer " + this.peer.getSuccessor2() + " has been killed.");
						System.out.println("My first successor is now peer " + this.peer.getSuccessor1());
						
						// If the second successor has been killed, ask the new first successor for its 
						// 	first successor
						this.peer.getNewSuccessor1(this.peer.getSuccessor2());
						modified = 1;
					}

					// If the peer's successors has been updated, refresh the number of 
					//	pings sent and received.
					if (modified == 1){
						this.pingNo1 = 0;
						this.pingNo2 = 0;
						this.sentPing1 = 0;
						this.sentPing2 = 0;
					}
				} catch (Exception e){

				}

			} catch (UnknownHostException e) {
				System.out.println("Unkown Host.");
			} catch (IOException e){
				System.out.println("Could not send ping.");
			} catch (InterruptedException e) {
				System.out.println("Could not sleep.");
			}

		}
   }

    // To increase the number of pings received by a successor
   	public void increasePing1(){
		this.pingNo1 = this.pingNo1 + 1;
	}

	public void increasePing2(){
		this.pingNo2 = this.pingNo2 + 1;
	}

	// Used to update the peer's successors
	public void updatePingRequest(Peer peer){
		this.peer = peer;
	}

	// Used to update whether a ping should be running
	public void updateRunningPing(int number){
		this.run = number;
	}

	// Used to retrieve the number of pings received by a successor
	public int getPingNo1(){
		return this.pingNo1;
	}

	public int getPingNo2(){
		return this.pingNo2;
	}
}

/*
 * UDPServer: The thread which is responsible for listening for UDP
 * 	messages (ie. ping requests and responses). The main thread
 * 	listens for incoming UDP messages. There is also a function
 *	which is used to send UDP messages.
 */ 

class UDPServer extends Thread {
	private Peer peer;

	UDPServer(Peer peer){
		this.peer = peer;
	}

	public void run(){
		while (true) {
			try {
			// Create a datagram packet to hold incomming UDP packet.
			DatagramPacket request = new DatagramPacket(new byte[1024], 1024);

			// Block until the host receives a UDP packet.
			// The UDP is always listening for packets
			this.peer.getUDPSocket().receive(request);

			// Retrieve the received UDP packet.
			String message= printData(request);

			// Split the message according to spaces
			String splitMessage[] = message.split(" ");
			String response;

			// Decide what type of message has been received
			// NOTE: This assumes valid input for messages containing the keywords
			// 	PING and RESPONSE
			// PING -> Ping request received
			if(message.contains("PING")){
				// Obtain the sender of the ping request, as well as whether the receiving
				// 	peer was the sending peer's first or second successor
				int from = Integer.parseInt(splitMessage[1]);
				int successor = Integer.parseInt(splitMessage[2]);

				// Acknowledge the received ping request
				System.out.println("A ping request message was received from Peer " + from + ".");

				// Create the response message to send back to the peer making the request
				response = "RESPONSE " + this.peer.getPeerID() + " " + successor + "\n";

				// Send the UDP ping packet
				sendUDPMessage(response, from);

			// RESPONSE -> Ping response received
			} else if(message.contains("RESPONSE")){
				// Obtain the sender of the response and whether it was the first or
				// 	second successor
				int from = Integer.parseInt(splitMessage[1]);
				int successor = Integer.parseInt(splitMessage[splitMessage.length-1]);

				// Acknouwledge the ping response
				System.out.println("A ping response has been received from Peer" + from + ".");

				// Increse the number of pings received
				if(successor == 1){
					peer.getPings().increasePing1();
				} else {
					peer.getPings().increasePing2();
				}
			} 

			} catch (IOException e) {

			} catch (Exception e){

			}

		}    

		}			
   

	// Function for the sending of UDP messages
	private void sendUDPMessage (String messageString, int destination) throws Exception{
		// Obtain the destination port address
		int destinationPort = destination + 5000;

		// Inetaddress is just the local machine
		InetAddress host = InetAddress.getByName("127.0.0.1");

		// Set up the bytes required for the message
		byte[] buf = new byte[1024];
		buf = messageString.getBytes();

		// Create a datagram packet to send outgoing UDP packet.
		DatagramPacket message = new DatagramPacket(buf, buf.length, host, destinationPort);

		// Send the UDP packet. Note that the connection is not closed afterwards
		this.peer.getUDPSocket().send(message);

   	}

    // Used to update the UDP server whenever a peer leaves or is killed
	public void updateUDPServer(Peer peer){
		this.peer = peer;
	}

	// Code obtained and adapted from:
	//	https://github.com/awadalaa/Socket-Programming-Java/blob/master/UDP-Pinger/PingServer.java
	private String printData(DatagramPacket request) throws Exception{
		// Obtain references to the packet's array of bytes.
		byte[] buf = request.getData();

		// Wrap the bytes in a byte array input stream,
		// so that you can read the data as a stream of bytes.
		ByteArrayInputStream bais = new ByteArrayInputStream(buf);

		// Wrap the byte array output stream in an input stream reader,
		// so you can read the data as a stream of characters.
		InputStreamReader isr = new InputStreamReader(bais);

		// Wrap the input stream reader in a bufferred reader,
		// so you can read the character data a line at a time.
		// (A line is a sequence of chars terminated by any combination of \r and \n.) 
		BufferedReader br = new BufferedReader(isr);

		// The message data is contained in a single line, so read this line.
		String line = br.readLine();

		// Print host address and data received from it.
		return line;
	}
}

/*
 * Peer: This is for peer creation and links up a peer object with its
 * 	successors, UDP server and TCP servers, as well as their respective
 *	sending sockets.
 */ 
class Peer{
	private int successor1;
	private int successor2;
	private int peerID;
	private UDPServer udpServer;
	private TCPServer tcpServer;
	private DatagramSocket UDPSocket;
	private ServerSocket TCPSocket;
	private PingRequest pings;

	Peer(int peerID, int successor1, int successor2) throws Exception{
	// Setting up the Peer
		this.peerID = peerID;
		this.successor1 = successor1;
		this.successor2 = successor2;

		// Create the UDP and TCP sockets for sending
		try{
			this.UDPSocket = new DatagramSocket(peerID + 5000);
			this.TCPSocket = new ServerSocket(peerID + 50000);
		} catch (SocketException e){

		}

		// Start up the UDP and TCP threads for listening
		this.udpServer = new UDPServer(this);
		udpServer.start();
		this.tcpServer = new TCPServer(this);
		tcpServer.start();

		// Sleep the thread to ensure that all TCP and UDP sockets are ready
		//  for sending and receiving.
		Thread.sleep(500);

		// Start the pinging thread.
		this.pings = new PingRequest(this);
		pings.start();
	}

	// Function called when a file request is made
	public void fileRequest(int fileNumber) throws Exception{
		// Hash the file number accordingly
		int hash = fileNumber % 256;
		// Compose the string message
		String message = "FILEREQUEST " + fileNumber + " HASH " + hash + " SOURCE " + this.peerID + " FROM + " +this.peerID + "\n";
		// Send the message via TCP
		sendTCPMessage(message, successor1);
		// Print ouf sent message 
		System.out.println("File request message for " + fileNumber + " has been sent to my successor.");
	}

	// Function used to send TCP messages. The socket is closed at the end, which is required by TCP
	//	connections
	public void sendTCPMessage (String messageString, int destination) throws Exception{
		// Set up the port
		int destinationPort = destination + 50000;
		// Set up the client
		Socket client = new Socket("127.0.0.1",destinationPort);
		// Send the message
		DataOutputStream sendToServer = new DataOutputStream(client.getOutputStream());
		sendToServer.writeBytes(messageString);
		// Close the connection
		client.close();
	}

	// Function used for peer departure
	public void closePeer() throws Exception{
		// Message to send
		String message = "DEPARTING " + this.peerID + " " + this.successor1 + " " + this.successor2 + "\n";
		sendTCPMessage(message, this.successor1);
		// Stop the pinging of successors
		this.pings.updateRunningPing(0);
	}

	// Function to obtain the new second successor
	public void getNewSuccessor2(int killed) throws Exception{
	String message = "SUCCESSOR2 " + killed + " " + this.peerID + "\n";
	sendTCPMessage(message, this.successor2);
	}

	// Function to obtain the new first successor
	public void getNewSuccessor1(int killed) throws Exception{
	String message = "SUCCESSOR2 " + killed + " " + this.peerID + "\n";
	sendTCPMessage(message, this.successor1);
	}

	public int getPeerID(){
	return this.peerID;
	}

	public int getSuccessor1(){
	return this.successor1;
	}

	public int getSuccessor2(){
	return this.successor2;
	}

	public void setSuccessor1(int num){
	this.successor1 = num;
	}

	public void setSuccessor2(int num){
	this.successor2 = num;
	}

	public DatagramSocket getUDPSocket(){
	return this.UDPSocket;
	}

	public ServerSocket getTCPSocket(){
	return this.TCPSocket;
	}

	public UDPServer getUDPServer(){
	return this.udpServer;
	}
	public PingRequest getPings(){
	return this.pings;
	}

	}


/*
 * TCPServer: Listens on the port which receives the TCP messages
 */
class TCPServer extends Thread{
   private Peer peer;

   TCPServer(Peer peer){
	  this.peer = peer;
   }
   
   public void run(){

	  while(true){
		 try{
			// Must first establish connection
			Socket client = null;
			client = this.peer.getTCPSocket().accept();

			if(client != null){
			   //  Get the message 
			   InputStream input = client.getInputStream();
			   BufferedReader br = new BufferedReader(new InputStreamReader(input));
			   String message = br.readLine();
			   String splitMessage[] = message.split(" ");
			   String response;
			   
			   // Case: Forwarding a file reuqest.
			   if (message.contains("FILEREQUEST")){
					int file = Integer.parseInt(splitMessage[1]);
					int hash = Integer.parseInt(splitMessage[3]);
					int source = Integer.parseInt(splitMessage[5]);

					// It's found if the peer has a matching ID, if the previous peer tells them they have it or if the hash lies between the source ID and the previous peer
					if(hash == this.peer.getPeerID() || message.contains("END") || (this.peer.getPeerID() < source && hash > Integer.parseInt(splitMessage[splitMessage.length-1]))){
						// Print out the response
						System.out.println("File " + file + " is stored here.");
						// Inform the source that the file has been found
						response = "FOUND " + file + " AT " + this.peer.getPeerID();
						System.out.println("A response message destined for peer " + source + ", has been sent.");
						// Send the response back
						this.peer.sendTCPMessage(response, source);

					} else {
						// Print out the response
						System.out.println("File " + file +" is not stored here.");
						System.out.println("File request message has been forwarded to my successor.");
						// Determine whether the next successor is the owner of the file (between the highest number peer and the lowest)
						// Else, just forward the message as it is
						if(this.peer.getSuccessor1() < this.peer.getPeerID() || (this.peer.getSuccessor1() > hash && this.peer.getSuccessor2() > hash) ||
							((this.peer.getSuccessor1() < this.peer.getPeerID()) && (hash > this.peer.getPeerID()))){
							String found = message + " END";
							this.peer.sendTCPMessage(found, this.peer.getSuccessor1());
						} else {
							this.peer.sendTCPMessage(message+ " " + this.peer.getPeerID(), this.peer.getSuccessor1());
						}
					} 
					
				// Case: File has been found.
				} else if (message.contains("FOUND") && message.contains("AT")){
					// Break down the message
					int file = Integer.parseInt(splitMessage[1]);
					String source = (splitMessage[splitMessage.length-1]);
					// Print out the response
					System.out.println("Received a response message from peer " + source + ", which has the file " + file + ".");

				// Case; Peer is departing.
				} else if (message.contains("DEPARTING")){
					int leaving = Integer.parseInt(splitMessage[1]);
					int newSuccessor1 = Integer.parseInt(splitMessage[2]);
					int newSuccessor2 = Integer.parseInt(splitMessage[3]);
					// If the seccond successor is leaving -> just update the second successor
					if(this.peer.getSuccessor2() == leaving){
						System.out.println("Peer " + leaving + " will depart from the network.");
						this.peer.setSuccessor2(newSuccessor1);
						System.out.println("My first successor is now peer " + this.peer.getSuccessor1());
						System.out.println("My second successor is now peer " + this.peer.getSuccessor2());
						this.peer.sendTCPMessage(message, this.peer.getSuccessor1());
						updateTCPServer(this.peer);
						this.peer.getUDPServer().updateUDPServer(this.peer);
						this.peer.getPings().updatePingRequest(this.peer);

					// If the first successor is leaving -> update the first and second successor
					} else if(this.peer.getSuccessor1() == leaving){
						System.out.println("Peer " + leaving + " will depart from the network.");
						this.peer.setSuccessor1(newSuccessor1);
						this.peer.setSuccessor2(newSuccessor2);
						System.out.println("My first successor is now peer " + this.peer.getSuccessor1());
						System.out.println("My second successor is now peer " + this.peer.getSuccessor2());
						updateTCPServer(this.peer);
						this.peer.getUDPServer().updateUDPServer(this.peer);
						this.peer.getPings().updatePingRequest(this.peer);
					} else {
						this.peer.sendTCPMessage(message, this.peer.getSuccessor1());
					}

				// Case: Trying to find second successor.
				} else if (message.contains("SUCCESSOR2") && !message.contains("FOUND")){
					int from = Integer.parseInt(splitMessage[2]);
					int killed = Integer.parseInt(splitMessage[1]);
					if(killed == this.peer.getSuccessor1()){
						response = message + " FOUND " + this.peer.getSuccessor2();
					} else {
						response = message + " FOUND " + this.peer.getSuccessor1();
					}
					this.peer.sendTCPMessage(response, from);

				// Case: Second successor found.
				} else if (message.contains("SUCCESSOR2")){
					int newSuccessor2 = Integer.parseInt(splitMessage[splitMessage.length-1]);
					System.out.println("My second successor is now peer " + newSuccessor2 + ".");
					this.peer.setSuccessor2(newSuccessor2);
					updateTCPServer(this.peer);
					this.peer.getUDPServer().updateUDPServer(this.peer);
					this.peer.getPings().updatePingRequest(this.peer);
				}
			}
		} catch (IOException e){

		} catch (Exception e){

		}
	}
}

   public void updateTCPServer(Peer peer){
	  this.peer = peer;
   }

}


